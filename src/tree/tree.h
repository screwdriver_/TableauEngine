#ifndef TREE_HPP
# define TREE_HPP

# include<string>

namespace TabEngine
{
	template<typename T>
	struct BSTNode
	{
		T value;
		BSTNode<T>* left;
		BSTNode<T>* right;
	};

	template<typename T>
	class BST
	{
		public:
			void linkFile(const string file);

			// TODO

		private:
			BSTNode<T>* root;
	};
}

#endif
