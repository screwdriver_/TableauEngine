# TableauEngine

TableauEngine (from French, "Tableau" = "Table") is a storing engine allowing to store data under the form of tables.



## Files structure

Data are stored in files under the following structure:

```
tables/					# Directory containing all the database's tables
	foo/				# The `foo` table's directory
		data/			# The directory containing the table's data
			0.bin		# A data file
		trees/			# The directory containing the B trees
			bar.bin		# The `bar` column's tree file
		files/			# The directory containing the table's files
			bar_0.data	# A file in the table (column: bar ; line: 0)
		deleted.bin		# The deletion register
		next_id.bin		# The next id stored in binary format
		structure.bin	# The table's structure
		table.json		# The informations about the table
database.json			# The informations about the database
```

The **database.json** file contains basic informations about the database:
- Name
- Description
- Owner
- Creation timestamp

The **table.json** file contains basic informations about the table:
- Name
- Description
- The amount of lines per data file (1024 by default)



# Data storage

Data are stored under binary format inside files of 1024 lines (by default).

TODO: Talk about storage of each lines in file

Tables have a predefined structure stored into the **structure.bin** file.
The structure defines the columns of the table. Every line in this table must fit those columns.

The following format is repeated for each column in the table.

| Name | Size (bytes) | Description                        |
|------|--------------|------------------------------------|
| name | *dynamic*    | The column's name                  |
|      | 1            | Name's end (``\0``)                |
| type | 1            | The data type                      |
| size | 2            | The size of a value in this column |



Available data types are the following:

| # | Name             | Size range (bytes) | Description                |
|---|------------------|--------------------|----------------------------|
| 0 | integer          | 1, 2, 4 or 8       | A signed integer number    |
| 1 | unsigned integer | 1, 2, 4 or 8       | An unsigned integer number |
| 2 | float            | 4 or 8             | A floating-point number    |
| 3 | boolean          | 1/8                | True or false              |
| 4 | string           | 1 to 65535         | A string                   |
| 5 | file             | 0 \*               | An external file           |

\* Files have a size of 0 because they are stored in a separated place.



# Search trees

TODO



# Files

TODO



# Line searching

The aim of a database is to optimize data searching and modifying operations. Thus the database uses various method to do so according to which parameter is provided to perform the research.

Searching can't be done using file type columns because files can be very large.



## By id

If the line id is provided, the database can directly get the corresponding line.

First, the database checks if the id is higher or equals the next id or if it's present in the deletion register. If it's the case, then the database returns no line.

Else, the database gets the file containing the line with the following operation: **id / lines\_per\_file**.
Then, the line's position can be recovered using the following operation: **id % lines\_per\_file \* line_size**.

This operation has a complexity of **O(1)**.



## By value

To research a line by value, the database uses the B trees of concerned columns.

The database goes through the tree of each column to find the lines that contain the appropriate values.

This operation has a complexity of **O(log n)**.



# Line insertion

To insert a line, the database first checks the deletion register. If it isn't empty, the database takes the first id and then removes it from the deletion register. Else it takes the next id and increments it.

Then, the database inserts the data with the id. (A new file is created if needed)



# Line updating

To update lines, the database just searches the lines to update and then writes the new values.



# Line deletion

To delete a line, the database just adds its id to the deletion register and removes it from all B trees.
