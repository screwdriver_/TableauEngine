NAME = TableauEngine
COMPILER = gcc
FLAGS = -Wall -Wextra -Werror -c

SRC = src/*.cpp

all: $(NAME)

$(NAME):
	@$(COMPILER) $(FLAGS) $(SRC)
	@ar rc $(NAME).a *.o

clean:
	@rm -rf *.o

fclean: clean
	@rm -rf $(NAME).a

re: fclean all
